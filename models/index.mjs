import dotenv from 'dotenv';
dotenv.config();

import { Sequelize } from 'sequelize';
export {default as Sequelize} from 'sequelize';

// ALL MODELS SHOULD BE IMPORTED THIS WAY
import taskSchema from './task.mjs';

export const sequelize = new Sequelize(
  process.env.MYSQL_DATABASE,
  process.env.MYSQL_USER,
  process.env.MYSQL_PASSWORD,
  {
    host: process.env.MYSQL_HOST,
    dialect: process.env.MYSQL_DIALECT,
    pool: {
      max: parseInt(process.env.MYSQL_SEQUELIZE_POOL_MAX),
      min: parseInt(process.env.MYSQL_SEQUELIZE_POOL_MIN),
      acquire: parseInt(process.env.MYSQL_SEQUELIZE_POOL_IDLE),
      idle: parseInt(process.env.MYSQL_SEQUELIZE_POOL_ACQUIRE)
    }
  }
);

// ALL MODELS SHOULD BE EXPORTED THIS WAY
export const taskModel = taskSchema(sequelize, Sequelize);