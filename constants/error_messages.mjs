export const REQUEST_ERROR = {
  USER_ID: "User id can't be empty",
  ID: "Invalid id",
  TITLE: "Title can't be empty",
  EMPTY: "Request must contain data"
}

export const PERMISSION_ERROR = {
  NOT_FROM_UID: "The task doesn't belong to the specified user id"
}

export const RESPONSE_ERROR = {
  NOT_FOUND: "The task id could not be found",
  NOT_MODIFIED: "The task could not be modified"
}