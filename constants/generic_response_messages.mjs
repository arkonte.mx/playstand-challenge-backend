export const GENERIC_INSERT_ERROR_MESSAGE = "Some error occurred while creating the task";
export const GENERIC_RETRIEVE_ERROR_MESSAGE = "Some error occurred while retreaving tasks";
export const GENERIC_UPDATE_ERROR_MESSAGE = "Some error occurred while updating tasks";
export const GENERIC_DELETE_ERROR_MESSAGE = "Some error occurred while deleting tasks";