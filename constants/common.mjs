export const ZERO = 0;
export const ONE = 1;
export const TWO = 2;

export const EMPTY_STRING = "";
export const UNDERSCORE = "_";
export const COMMA = ",";

export const WITH = "with";
export const ID = "id";

export const OLD = "old";
export const NEW = "new";

export const TITLE = "title";
export const DESCRIPTION = "description";