import { Router } from 'express';
import * as Actions from '../controllers/index.mjs';

export default (app) => {
  const router = Router();

  router.route("/todos")
    .get(Actions.findAllTasks)
    .post(Actions.createTask);

  router.route("/todos/:id")
    .get(Actions.findTaskById)
    .put(Actions.updateTask)
    .patch(Actions.completeTask)
    .delete(Actions.removeTask)

  router.route("/todos/title/:title")
    .get(Actions.findTaskByTitle);

  app.use("/", router);
};