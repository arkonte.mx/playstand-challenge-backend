import dotenv from 'dotenv';
dotenv.config();

import chai from 'chai';
import chaiHttp from 'chai-http';
import { OK, BAD_REQUEST } from './constants/http_response_status_codes.mjs';
import { ZERO, EMPTY_STRING, UNDERSCORE, OLD, NEW, TITLE, DESCRIPTION } from './constants/common.mjs';

chai.use(chaiHttp);
const expect = chai.expect;
const HOST = process.env.ROOT_URL;

describe("Basic CRUD testing", () => {

  const date_time_as_iso = (new Date()).toISOString();
  const OLD_TEST_TASK_TITLE = `${OLD}${UNDERSCORE}${TITLE}${UNDERSCORE}${date_time_as_iso}`;
  const NEW_TEST_TASK_TITLE = `${NEW}${UNDERSCORE}${TITLE}${UNDERSCORE}${date_time_as_iso}`;
  const NEW_TEST_TASK_DESCRIPTION = `${NEW}${UNDERSCORE}${DESCRIPTION}${UNDERSCORE}${date_time_as_iso}`;
  const TEST_UID = "user_id";

  let test_task_id = 0;

  it("should fail on creating task by empty title", (done) => {
    const data = { title: EMPTY_STRING, uid: TEST_UID };

    chai.request(HOST)
      .post("/todos")
      .send(data)
      .end((error = {}, response = {}) => {
        expect(response).to.have.status(BAD_REQUEST);
        done();
      });
  });

  it("should create task successfully", (done) => {
    const data = { title: OLD_TEST_TASK_TITLE, uid: TEST_UID };

    chai.request(HOST)
      .post("/todos")
      .send(data)
      .end((error = {}, response = {}) => {
        const { body = {} } = response;

        expect(response).to.have.status(OK);

        if (body.id > ZERO) {
          test_task_id = body.id;
        }

        done();
      });
  });

  it("should retrieve all tasks", (done) => {
    const query = { uid: TEST_UID };

    chai.request(HOST)
      .get("/todos/")
      .query(query)
      .end((error = {}, response = {}) => {
        expect(response).to.have.status(OK);
        expect(response.body).to.have.lengthOf.above(ZERO);
        done();
      });
  });

  it("should retrieve task by id", (done) => {
    const query = { id: test_task_id, uid: TEST_UID };

    chai.request(HOST)
      .get("/todos/:id")
      .query(query)
      .end((error = {}, response = {}) => {
        const { body = {} } = response;

        expect(response).to.have.status(OK);
        expect(body.id).to.equal(test_task_id);

        done();
      });
  });

  it("should retrieve tasks by title", (done) => {
    const query = { title: OLD_TEST_TASK_TITLE, uid: TEST_UID };

    chai.request(HOST)
      .get("/todos/title/:title")
      .query(query)
      .end((error = {}, response = {}) => {
        const { body: [ task = {} ] } = response;

        expect(response).to.have.status(OK);
        expect(task.title).to.equal(OLD_TEST_TASK_TITLE);

        done();
      });
  });

  it("should update task properties by id", (done) => {
    const data = { id: test_task_id, title: NEW_TEST_TASK_TITLE, description: NEW_TEST_TASK_DESCRIPTION, uid: TEST_UID };

    chai.request(HOST)
      .put("/todos/:id")
      .send(data)
      .end((error = {}, response = {}) => {
        const { body: { message = EMPTY_STRING } } = response;

        expect(response).to.have.status(OK);

        done();
      });
  });

  it("should set task as complete by id", (done) => {
    const data = { id: test_task_id, uid: TEST_UID };

    chai.request(HOST)
      .patch("/todos/:id")
      .send(data)
      .end((error = {}, response = {}) => {
        const { body: { message = EMPTY_STRING } } = response;

        expect(response).to.have.status(OK);

        done();
      });
  });

  it("should delete task by id", (done) => {
    const data = { id: test_task_id, uid: TEST_UID };

    chai.request(HOST)
      .delete("/todos/:id")
      .send(data)
      .end((error = {}, response = {}) => {
        const { body: { message = EMPTY_STRING } } = response;

        expect(response).to.have.status(OK);

        done();
      });
  });

});