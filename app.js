import express from 'express';
import cors from 'cors';

import { taskRoutes } from './routes/index.mjs';
import { sequelize as taskSequelize } from './models/index.mjs';

const PORT = process.env.NODE_LOCAL_PORT;

const cors_options = {
  origin: process.env.CORS_URL
};

const app = express();
app.use(cors(cors_options));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

taskRoutes(app);

// TO START THE APPLICATION WITH "CLEAN" TABLES
taskSequelize.sync({ force: true });

app.get("/", (request, response) => {
  response.json({ message: "Todos challenge is up and running :D" });
});

app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}.`);
});