import { ZERO, ONE, TWO, EMPTY_STRING } from '../constants/common.mjs';
import { REQUEST_ERROR, PERMISSION_ERROR, RESPONSE_ERROR } from '../constants/error_messages.mjs';
import { BAD_REQUEST, FORBIDDEN, NO_CONTENT } from '../constants/http_response_status_codes.mjs';

class CheckupExcepcion extends Error {
  constructor(type, message) {
    super(message);

    this.name = "CheckupExcepcion";
    this.type = type;
    this.message = message;
  }

}

export const checkUpTaskRequest = (request_data = {}) => {

  const validations = {
    uid: (data = {}) => {
      const { uid } = data.request_data;

      return (!uid || uid.trim() === EMPTY_STRING)
        && REQUEST_ERROR.USER_ID
    },
    id: (data = {}) => {
      const { id } = data.request_data;

      return (typeof id === "number" && id < ONE)
        && REQUEST_ERROR.ID
    },
    title: (data = {}) => {
      const { title } = data.request_data;

      return (typeof title === "string" && title.trim() === EMPTY_STRING)
        && REQUEST_ERROR.TITLE
    },
    empty: (data = {}) => {
      const { request_data = {} } = data;
      const { uid, ...rest_of_request_data } = request_data;

      return (Object.keys(request_data).length > ONE && Object.keys(rest_of_request_data).length < ONE)
        && REQUEST_ERROR.EMPTY
    }
  };

  const data = { request_data };
  runTaskCheckup(BAD_REQUEST, validations, data);
}

export const checkUpTaskPermissions = (request_data = {}, stored_data = {}) => {

  const validations = {
    uid: (data = {}) => {
      const { uid: request_uid } = data.request_data;
      const { uid: stored_uid } = data.stored_data;

      return (!request_uid || !stored_uid || (request_uid !== stored_uid))
        && PERMISSION_ERROR.NOT_FROM_UID
    }
  };

  const data = { request_data, stored_data };
  runTaskCheckup(FORBIDDEN, validations, data);
}

export const checkUpTaskResponse = (stored_data, changed_rows_count) => {

  const validations = {
    not_found: (data = {}) => {
      const { id = ZERO, title = EMPTY_STRING } = data.stored_data;

      return ((!id || !title) || (id < ONE || title.trim() === EMPTY_STRING))
        && RESPONSE_ERROR.NOT_FOUND
    },
    not_modified: (data = {}) => {
      const { changed_rows_count } = data;

      return (typeof changed_rows_count === "number" && changed_rows_count < ONE)
        && RESPONSE_ERROR.NOT_MODIFIED
    }
  };

  const data = { stored_data, changed_rows_count };
  runTaskCheckup(NO_CONTENT, validations, data);
}

const runTaskCheckup = (type = EMPTY_STRING, validations = [], data = {}) => {
  let error_message = null;

  for (const validation_name of Object.keys(validations)) {
    const validation = validations[validation_name] || (() => {});

    error_message = validation(data);

    if(error_message) {
      break;
    }
  }

  if(error_message) {
    throw new CheckupExcepcion(type, error_message);
  }
}