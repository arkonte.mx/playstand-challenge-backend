import { ZERO, EMPTY_STRING } from '../constants/common.mjs';
import { GENERIC_INSERT_ERROR_MESSAGE, GENERIC_RETRIEVE_ERROR_MESSAGE, GENERIC_UPDATE_ERROR_MESSAGE, GENERIC_DELETE_ERROR_MESSAGE } from '../constants/generic_response_messages.mjs';
import { INTERNAL_SERVER_ERROR } from '../constants/http_response_status_codes.mjs';
import { checkUpTaskRequest, checkUpTaskPermissions, checkUpTaskResponse } from '../utils/checkups.mjs';

import { Sequelize, taskModel } from '../models/index.mjs';

const Op = Sequelize.Op;

export const createTask = async (request, result) => {
  const { body = {} } = request;
  const { title = EMPTY_STRING, description = EMPTY_STRING, uid = EMPTY_STRING } = body;

  try {

    checkUpTaskRequest(body);
    const { dataValues: task = {} } = await taskModel.create({ title, description, uid });

    checkUpTaskResponse(task);
    result.send(task);

  } catch ({ type = INTERNAL_SERVER_ERROR, message = GENERIC_INSERT_ERROR_MESSAGE }) {
    result.status(type).send({ message });
  }
}

export const findAllTasks = (request, result) => {
  const { query = {} } = request;
  const by = {};
  findBy(query, by, result);
}

export const findTaskByTitle = (request, result) => {
  const { query = {} } = request;
  const by = { title: { [Op.like]: `%${query.title}%` } };
  findBy(query, by, result);
}

export const findTaskByCompleted = (request, result) => {
  const { query = {} } = request;
  const by = { completed: query.completed };
  findBy(query, by, result);
}

const findBy = async (query, where, result) => {
  try {

    checkUpTaskRequest(query);
    const tasks = await taskModel.findAll({ ...where, uid: query.uid });
    result.send(tasks);

  } catch ({ type = INTERNAL_SERVER_ERROR, message = GENERIC_RETRIEVE_ERROR_MESSAGE }) {
    result.status(type).send({ message });
  }
}

export const findTaskById = async (request, result) => {
  const { query = {} } = request;

  try {

    checkUpTaskRequest(query);
    const { dataValues: task = {} } = await taskModel.findByPk(query.id);
    checkUpTaskPermissions(query, task);

    checkUpTaskResponse(task);
    result.send(task);

  } catch ({ type = INTERNAL_SERVER_ERROR, message = GENERIC_RETRIEVE_ERROR_MESSAGE }) {
    result.status(type).send({ message });
  }
}

export const updateTask = async (request, result) => {
  const { body = {} } = request;
  const { id = ZERO, title = EMPTY_STRING, description = EMPTY_STRING, completed = false } = body;

  try {

    checkUpTaskRequest(body);
    const { dataValues: task = {} } = await taskModel.findByPk(id);
    checkUpTaskPermissions(body, task);

    const update = { title, description, completed };
    const [ update_count ] = await taskModel
      .update(update, { where: { id } });

    const response = { ...task, ...update };
    checkUpTaskResponse(response, update_count);
    result.send(response);

  } catch ({ type = INTERNAL_SERVER_ERROR, message = GENERIC_UPDATE_ERROR_MESSAGE }) {
    result.status(type).send({ message });
  }
}

export const completeTask = async (request, result) => {
  const { body = {} } = request;
  const { id = ZERO } = body;

  try {

    checkUpTaskRequest(body);
    const { dataValues: task = {} } = await taskModel.findByPk(id);
    checkUpTaskPermissions(body, task);

    const update = { completed: !task.completed };
    const [ complete_count ] = await taskModel
      .update(update, { where: { id } });

    const response = { ...task, ...update };
    checkUpTaskResponse(response, complete_count);
    result.send(response);

  } catch ({ type = INTERNAL_SERVER_ERROR, message = GENERIC_UPDATE_ERROR_MESSAGE }) {
    result.status(type).send({ message });
  }
}

export const removeTask = async (request, result) => {
  const { body = {} } = request;
  const { id = ZERO } = body;

  try {

    checkUpTaskRequest(body);
    const { dataValues: response = {} } = await taskModel.findByPk(id);
    checkUpTaskPermissions(body, response);

    const destroy_count = await taskModel
      .destroy({ where: { id } });

    checkUpTaskResponse(response, destroy_count);
    result.send(response);

  } catch ({ type = INTERNAL_SERVER_ERROR, message = GENERIC_DELETE_ERROR_MESSAGE }) {
    result.status(type).send({ message });
  }
}

export const removeAllTasks = async (request) => {
  const { body = {} } = request;
  const { uid = EMPTY_STRING } = body;

  try {

    checkUpTaskRequest(body);
    const response = await taskModel.destroy({
      returning: true,
      where: { uid }
    });

    result.send(response);

  } catch ({ type = INTERNAL_SERVER_ERROR, message = GENERIC_DELETE_ERROR_MESSAGE }) {
    result.status(type).send({ message });
  }
}