# PayStand Challenge: Backend
## Setup
Before setting up the app, make sure you already have [Docker](https://docs.docker.com/get-docker/) installed on your system.

After cloning the repository, in order to run the backend you must execute the following command inside the root folder of the proyect:
### For Windows
```bash
> setup_on_windows.bat
```
### For Linux
```sh
$ sh setup_on_linux.sh
```

##### **Once the setup is complete the [api](http://localhost:4000) will run on port 4000**

## Tests
Unit tests for basic CRUD operations were added to the proyect, to run them simply execute this command **after the containers are up an running**:
```sh
$ npm test
```
## Frontend Application
Once the backend is ready, please setup the [PayStand Challenge Frontend](https://gitlab.com/arkonte.mx/playstand-challenge-frontend.git)

## Errata
### Windows
If the setup fails, please do the following:
- Open Docker Desktop dashboard
- Delete the project's container
- Go to Settings
  - Click on "Resources"
    - Click on "FILE SHARING"
      - Add the project's folder to the list of directories
- Run the setup again